// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <vector>
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Maze_hwGameModeBase.generated.h"

USTRUCT(BlueprintType)
struct FCellCoordinates
{
	GENERATED_BODY()

	int32 x = 0;
	int32 y = 0;
};

USTRUCT(BlueprintType)
struct FCell
{
	GENERATED_BODY()

	int32 CoordinatX = 0;
	int32 CoordinatY = 0;
	bool Open_Top = false;
	bool Open_Right = false;
	bool Open_Down = false;
	bool Open_Left = false;

	bool bCellIsBusy = false;
};


USTRUCT(BlueprintType)
struct FBlockingCell
{
	GENERATED_BODY()

	bool Block_Top = false;
	bool Block_Right = false;
	bool Block_Down = false;
	bool Block_Left = false;
};

UCLASS()
class MAZE_HW_API AMaze_hwGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cell")
	TSubclassOf<class AMazeWall> MazeWall;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cell", meta=(ClampMin = "2", ClampMax = "100"))
	int32 FieldWidth = 10;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cell", meta=(ClampMin = "2", ClampMax = "100"))
	int32 FieldLength = 10;
	int32 SizeOneBlock = 180;
	
	FCellCoordinates StartCoordinat;
	FCellCoordinates FinishCoordinat;

	FCellCoordinates CurrentCoordinat;
	FCellCoordinates NextCoordinat;

	int32 CountThreads = 3;
	TArray<FCell> AllCells;
	TArray<FCell> AllCellsThread;

	UFUNCTION()
	void FillAllCells();
	
	//UFUNCTION()
	void FillCurrentCells(const FCell& CurrentCell);

	FCell* GetCell(int32 CellCoordinatX, int32 CellCoordinatY);
	void GetNextCell(int32 CurrentCoordinatX, int32 CurrentCoordinatY, std::vector<FCell>* nextStep);
	void GetNextCell(int32 CoordinatX, int32 CoordinatY, std::vector<FCellCoordinates>* NextSteps);
	void GetNextCell_v3(int32 CoordinatX, int32 CoordinatY, std::vector<FCellCoordinates>* NextSteps);

	UFUNCTION()
	void FindPath();

	UFUNCTION()
	void FindPath_v2();

	UFUNCTION()
	void FillBorder();

	UFUNCTION()
	void FillOneSide(int32 Fl, int32 Fw, int32 CoordinateOffset = 0);

	UFUNCTION()
	void FillWallsForAllCells();

	//====================================================
	UFUNCTION()
	//void FillAllCells_v3(TArray<FCell> AllCellsThread);
	void FillAllCells_v3();
	
	UFUNCTION()
	//void FindPath_v3(TArray<FCell> AllCellsThread);
	void FindPath_v3();

	//FCell* GetCell(int32 CellCoordinatX, int32 CellCoordinatY, TArray<FCell> AllCellsThread);
	FCell* GetCell_v3(int32 CellCoordinatX, int32 CellCoordinatY);
	
	UFUNCTION()
	void FillBorder_v3(int32 CoordinateOffset);
		
	UFUNCTION()
	//void FillWallsForAllCells_v3(int32 CoordinateOffset, TArray<FCell> AllCellsThread);
	void FillWallsForAllCells_v3(int32 CoordinateOffset);
	
};
