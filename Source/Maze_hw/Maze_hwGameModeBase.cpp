// Copyright Epic Games, Inc. All Rights Reserved.


#include "Maze_hwGameModeBase.h"
#include <random>
#include <stack>
#include "MazeWall.h"

void AMaze_hwGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	//start default
	StartCoordinat.x = 1;
	StartCoordinat.y = 1;
	//finish default
	FinishCoordinat.x = FieldWidth;
	FinishCoordinat.y = FieldLength;

	//FindPath();
	//FindPath_v2();

	AsyncTask(ENamedThreads::GameThread,
		[&]()
		{
			// for (int32 i = 0; i < (CountThreads - 1); ++i)
			//  // while (i > -1)
			//  {
			//  // 	i--;
			// 	// TArray<FCell> AllCellsThread;
			//  	const int32 CoordinateOffset = i* (FieldWidth * SizeOneBlock + 200);
			// 	FillBorder_v3(CoordinateOffset);
			//
			// 	// FillAllCells_v3(AllCellsThread);
			// 	// FindPath_v3(AllCellsThread);
			// 	// FillWallsForAllCells_v3(CoordinateOffset, AllCellsThread);
			// 	
			// 	FillAllCells_v3();
			// 	FindPath_v3();
			// 	FillWallsForAllCells_v3(CoordinateOffset);
			// };
			// ========================================================
			// FillBorder_v3(0);
			// FillAllCells_v3();
			// FindPath_v3();
			// FillWallsForAllCells_v3(0);
			//
			// AllCellsThread.Reset();
			// FillBorder_v3(2000);
			// FillAllCells_v3();
			// FindPath_v3();
			// FillWallsForAllCells_v3(2000);

			// ========================================================
			int32 i = 0;
			while (i < CountThreads)
			{
				int32 CoordinateOffset = i * (FieldWidth * SizeOneBlock + 200);

				AllCellsThread.Reset();
				FillBorder_v3(CoordinateOffset);
				FillAllCells_v3();
				FindPath_v3();
				FillWallsForAllCells_v3(CoordinateOffset);
				i++;
			}
		});
	
}

void AMaze_hwGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AMaze_hwGameModeBase::FillAllCells()
{
	for (int32 x = 1; x <= FieldWidth; x++)
	{
		for (int32 y = 1; y <= FieldLength; y++)
		{
			FCell NewCell;
			NewCell.CoordinatX = x;
			NewCell.CoordinatY = y;
			NewCell.Open_Top = false;
			NewCell.Open_Right = false;
			NewCell.Open_Down = false;
			NewCell.Open_Left = false;
			NewCell.bCellIsBusy = false;
			AllCells.Add(NewCell);
		}
	}
}

void AMaze_hwGameModeBase::FillAllCells_v3()
{
	for (int32 x = 1; x <= FieldWidth; x++)
	{
		for (int32 y = 1; y <= FieldLength; y++)
		{
			FCell NewCell;
			NewCell.CoordinatX = x;
			NewCell.CoordinatY = y;
			NewCell.Open_Top = false;
			NewCell.Open_Right = false;
			NewCell.Open_Down = false;
			NewCell.Open_Left = false;
			NewCell.bCellIsBusy = false;
			AllCellsThread.Add(NewCell);
		}
	}
}

void AMaze_hwGameModeBase::FillCurrentCells(const FCell& CurrentCell)
{
	FCell* CellForFill = GetCell(CurrentCell.CoordinatX, CurrentCell.CoordinatY);
	CellForFill->Open_Top = CellForFill->Open_Top || CurrentCell.Open_Top;
	CellForFill->Open_Right = CellForFill->Open_Right || CurrentCell.Open_Right;
	CellForFill->Open_Down = CellForFill->Open_Down || CurrentCell.Open_Down;
	CellForFill->Open_Left = CellForFill->Open_Left || CurrentCell.Open_Left;
	CellForFill->bCellIsBusy = true;
}

FCell* AMaze_hwGameModeBase::GetCell(int32 CellCoordinatX, int32 CellCoordinatY)
{
	FCell* FoundCell = nullptr;

	// for (auto CurrCell : AllCells)
	// {
	// 	if (CurrCell.CoordinatX == CellCoordinatX && CurrCell.CoordinatY == CellCoordinatY)
	// 	{
	// 		FoundCell = &CurrCell;
	// 		break;
	// 	}
	// }

	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int i = 0; i <= totalCells; i++)
	{
		if (AllCells[i].CoordinatX == CellCoordinatX && AllCells[i].CoordinatY == CellCoordinatY)
		{
			FoundCell = &AllCells[i];
			break;
		}
	}

	return FoundCell;
}

FCell* AMaze_hwGameModeBase::GetCell_v3(int32 CellCoordinatX, int32 CellCoordinatY)
{
	FCell* FoundCell = nullptr;

	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int i = 0; i <= totalCells; i++)
	{
		if (AllCellsThread[i].CoordinatX == CellCoordinatX && AllCellsThread[i].CoordinatY == CellCoordinatY)
		{
			FoundCell = &AllCellsThread[i];
			break;
		}
	}

	return FoundCell;
}

void AMaze_hwGameModeBase::GetNextCell(int32 CoordinatX, int32 CoordinatY, std::vector<FCell>* NextSteps)
{
	FCell CheckNextCell = *GetCell(CoordinatX, CoordinatY);

	if (!CheckNextCell.bCellIsBusy)
	{
		FCell NextCell;
		NextCell.CoordinatX = CoordinatX;
		NextCell.CoordinatY = CoordinatY;
		NextSteps->push_back(NextCell);
	}
}

void AMaze_hwGameModeBase::GetNextCell(int32 CoordinatX, int32 CoordinatY, std::vector<FCellCoordinates>* NextSteps)
{
	FCell CheckNextCell = *GetCell(CoordinatX, CoordinatY);

	if (!CheckNextCell.bCellIsBusy)
	{
		FCellCoordinates NextCell;
		NextCell.x = CoordinatX;
		NextCell.y = CoordinatY;
		NextSteps->push_back(NextCell);
	}
}

void AMaze_hwGameModeBase::GetNextCell_v3(int32 CoordinatX, int32 CoordinatY, std::vector<FCellCoordinates>* NextSteps)
{
	FCell CheckNextCell = *GetCell_v3(CoordinatX, CoordinatY);

	if (!CheckNextCell.bCellIsBusy)
	{
		FCellCoordinates NextCell;
		NextCell.x = CoordinatX;
		NextCell.y = CoordinatY;
		NextSteps->push_back(NextCell);
	}
}

void AMaze_hwGameModeBase::FindPath()
{
	//Cell Maze[FieldWidth][FieldLength];
	std::stack<FCell> path;
	// path.push(Maze[StartCoordinat.x][StartCoordinat.y]);
	FCell NewCell;
	NewCell.CoordinatX = StartCoordinat.x;
	NewCell.CoordinatY = StartCoordinat.y;
	path.push(NewCell);

	int32 CurrentCoordinatX = 0;
	int32 CurrentCoordinatY = 0;
	int32 StepBuild = 0;

	while (!path.empty())
	{
		StepBuild++;
		if (FieldWidth * FieldLength * 2 < StepBuild)
		{
			break;
		}

		FCell LastCell = path.top();
		CurrentCoordinatX = LastCell.CoordinatX;
		CurrentCoordinatY = LastCell.CoordinatY;

		//blocking unnecessary or busy directions
		FBlockingCell _BlockCell;
		int32 NextCoordinatX = 0;
		int32 NextCoordinatY = 0;
		FCell CheckCell;

		_BlockCell.Block_Top = false;
		_BlockCell.Block_Right = false;
		_BlockCell.Block_Down = false;
		_BlockCell.Block_Left = false;
		if (LastCell.CoordinatY == 1)
		{
			_BlockCell.Block_Top = true;
		}
		else if (LastCell.CoordinatY != FieldLength)
		{
			//TOP
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			//NextCoordinatX = NextCoordinatX;
			// NextCoordinatY = --NextCoordinatY;
			--NextCoordinatY;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Top = true;
			}

			//DOWN
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			//NextCoordinatX = NextCoordinatX;
			// NextCoordinatY = ++NextCoordinatY;
			++NextCoordinatY;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Down = true;
			}
		}
		else if (LastCell.CoordinatY == FieldLength)
		{
			_BlockCell.Block_Down = true;
		}

		if (LastCell.CoordinatX == 1)
		{
			_BlockCell.Block_Left = true;
		}
		else if (LastCell.CoordinatX != FieldWidth)
		{
			//RIGHT
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			//NextCoordinatX = ++NextCoordinatX;
			++NextCoordinatX;
			//NextCoordinatY = NextCoordinatY;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Right = true;
			}

			//LEFT
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			// NextCoordinatX = --NextCoordinatX;
			--NextCoordinatX;
			//NextCoordinatY = NextCoordinatY;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Left = true;
			}
		}
		if (LastCell.CoordinatX == FieldWidth)
		{
			_BlockCell.Block_Right = true;
		}

		//Let's try to collect options for the next direction
		std::vector<FCell> NextSteps;

		//TOP
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Top)
		{
			// NextCoordinatY = --NextCoordinatY;
			--NextCoordinatY;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//RIGHT
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Right)
		{
			// NextCoordinatX = ++NextCoordinatX;
			++NextCoordinatX;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//DOWN
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Down)
		{
			// NextCoordinatY = ++NextCoordinatY;
			++NextCoordinatY;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//LEFT
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Left)
		{
			// NextCoordinatX = --NextCoordinatX;
			--NextCoordinatX;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//select the next cell, otherwise go back and look for the next cell again
		if (!NextSteps.empty())
		{
			//выбираем сторону из возможных вариантов
			FCell next = NextSteps[rand() % NextSteps.size()];

			FCell CurrentCell = FCell();
			CurrentCell.CoordinatX = CurrentCoordinatX;
			CurrentCell.CoordinatY = CurrentCoordinatY;

			FCell NextCell = FCell();
			NextCell.CoordinatX = next.CoordinatX;
			NextCell.CoordinatY = next.CoordinatY;

			//Открываем сторону, в которую пошли на ячейках
			if (next.CoordinatX != LastCell.CoordinatX)
			{
				if (LastCell.CoordinatX - next.CoordinatX > 0)
				{
					CurrentCell.Open_Left = true;
					NextCell.Open_Right = true;
				}
				else
				{
					NextCell.Open_Left = true;
					CurrentCell.Open_Right = true;
				}
			}

			if (next.CoordinatY != LastCell.CoordinatY)
			{
				if (LastCell.CoordinatY - next.CoordinatY > 0)
				{
					CurrentCell.Open_Top = true;
					NextCell.Open_Down = true;
				}
				else
				{
					CurrentCell.Open_Down = true;
					NextCell.Open_Top = true;
				}
			}

			FillCurrentCells(CurrentCell);
			FillCurrentCells(NextCell);

			path.push(next);
		}
		else
		{
			//если пойти никуда нельзя, возвращаемся к предыдущему узлу
			path.pop();
		}
	}
}

void AMaze_hwGameModeBase::FindPath_v2()
{
	std::stack<FCellCoordinates> path;
	path.push(StartCoordinat);

	while (!path.empty())
	{
		FCellCoordinates LastCell = path.top();
		CurrentCoordinat = LastCell;

		std::vector<FCellCoordinates> ArrayNextSteps;
		FCell CheckCell;

		//TOP
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.y != 1)
		{
			--NextCoordinat.y;
			CheckCell = *GetCell(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}
		
		//DOWN
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.y != FieldLength)
		{
			++NextCoordinat.y;
			CheckCell = *GetCell(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}

		//RIGHT
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.x != FieldWidth)
		{
			++NextCoordinat.x;
			CheckCell = *GetCell(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}
		
		//LEFT
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.x != 1)
		{
			--NextCoordinat.x;
			CheckCell = *GetCell(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}

		if (!ArrayNextSteps.empty())
		{
			FCellCoordinates NextStep = ArrayNextSteps[rand() % ArrayNextSteps.size()];
			path.push(NextStep);

			FCell* CurrentCell = GetCell(CurrentCoordinat.x, CurrentCoordinat.y);
			FCell* NextCell = GetCell(NextStep.x, NextStep.y);
			
			if (NextStep.x != LastCell.x)
			{
				if (LastCell.x - NextStep.x > 0)
				{
					CurrentCell->Open_Left = true;
					NextCell->Open_Right = true;
					
					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
				else
				{
					NextCell->Open_Left = true;
					CurrentCell->Open_Right = true;

					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
			}

			if (NextStep.y != LastCell.y)
			{
				if (LastCell.y - NextStep.y > 0)
				{
					CurrentCell->Open_Top = true;
					NextCell->Open_Down = true;

					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
				else
				{
					NextCell->Open_Top = true;
					CurrentCell->Open_Down = true;

					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
			}
		}
		else
		{
			path.pop();
		}	
	}
}

void AMaze_hwGameModeBase::FindPath_v3()
{
	std::stack<FCellCoordinates> path;
	path.push(StartCoordinat);

	while (!path.empty())
	{
		FCellCoordinates LastCell = path.top();
		CurrentCoordinat = LastCell;

		std::vector<FCellCoordinates> ArrayNextSteps;
		FCell CheckCell;

		//TOP
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.y != 1)
		{
			--NextCoordinat.y;
			CheckCell = *GetCell_v3(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell_v3(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}
		
		//DOWN
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.y != FieldLength)
		{
			++NextCoordinat.y;
			CheckCell = *GetCell_v3(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell_v3(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}

		//RIGHT
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.x != FieldWidth)
		{
			++NextCoordinat.x;
			CheckCell = *GetCell_v3(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell_v3(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}
		
		//LEFT
		NextCoordinat = CurrentCoordinat;
		if (CurrentCoordinat.x != 1)
		{
			--NextCoordinat.x;
			CheckCell = *GetCell_v3(NextCoordinat.x, NextCoordinat.y);
			if (!CheckCell.bCellIsBusy)
			{
				GetNextCell_v3(NextCoordinat.x, NextCoordinat.y, &ArrayNextSteps);
			}
		}

		if (!ArrayNextSteps.empty())
		{
			FCellCoordinates NextStep = ArrayNextSteps[rand() % ArrayNextSteps.size()];
			path.push(NextStep);

			FCell* CurrentCell = GetCell_v3(CurrentCoordinat.x, CurrentCoordinat.y);
			FCell* NextCell = GetCell_v3(NextStep.x, NextStep.y);
			
			if (NextStep.x != LastCell.x)
			{
				if (LastCell.x - NextStep.x > 0)
				{
					CurrentCell->Open_Left = true;
					NextCell->Open_Right = true;
					
					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
				else
				{
					NextCell->Open_Left = true;
					CurrentCell->Open_Right = true;

					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
			}

			if (NextStep.y != LastCell.y)
			{
				if (LastCell.y - NextStep.y > 0)
				{
					CurrentCell->Open_Top = true;
					NextCell->Open_Down = true;

					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
				else
				{
					NextCell->Open_Top = true;
					CurrentCell->Open_Down = true;

					CurrentCell->bCellIsBusy = true;
					NextCell->bCellIsBusy = true;
				}
			}
		}
		else
		{
			path.pop();
		}	
	}
}

void AMaze_hwGameModeBase::FillBorder()
{
	FRotator RotationWallFl = FRotator(0, 0, 0);
	for (int fl = 0; fl < FieldLength; ++fl)
	{
		//float InXfl = 0.f + CoordinateOffset + (200 * (CoordinateOffset == 0) ? 0 : 1);
		float InXfl = 0.f;
		float InYfl = 0.f;
		
		for (int fw = 0; fw < FieldWidth; ++fw)
		{
			if (fw == 0 || fw == FieldWidth - 1)
			{
				InXfl = fl * SizeOneBlock + (SizeOneBlock / 2);
				if (fw == 0)
				{
					InYfl = fw * SizeOneBlock;
				}
				else
				{
					InYfl = fw * SizeOneBlock + SizeOneBlock;
				}

				const auto World = GetWorld();
				if (World && MazeWall)
				{
					FVector LocationWall = FVector(InXfl, InYfl, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
					                                                    FActorSpawnParameters());
				}
				FillOneSide(fl, fw);
			}
			else
			{
				FillOneSide(fl, fw);
			}
		}
	}
}

void AMaze_hwGameModeBase::FillBorder_v3(int32 CoordinateOffset)
{
	FRotator RotationWallFl = FRotator(0, 0, 0);
	for (int fl = 0; fl < FieldLength; ++fl)
	{
		//float InXfl = 0.f + CoordinateOffset + (200 * (CoordinateOffset == 0) ? 0 : 1);
		float InXfl = 0.f;
		float InYfl = 0.f;
		
		for (int fw = 0; fw < FieldWidth; ++fw)
		{
			if (fw == 0 || fw == FieldWidth - 1)
			{
				InXfl = (fl * SizeOneBlock + (SizeOneBlock / 2)) + CoordinateOffset;
				if (fw == 0)
				{
					InYfl = fw * SizeOneBlock;
				}
				else
				{
					InYfl = fw * SizeOneBlock + SizeOneBlock;
				}

				const auto World = GetWorld();
				if (World && MazeWall)
				{
					FVector LocationWall = FVector(InXfl, InYfl, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
																		FActorSpawnParameters());
				}
				FillOneSide(fl, fw, CoordinateOffset);
			}
			else
			{
				FillOneSide(fl, fw, CoordinateOffset);
			}
		}
	}
}

void AMaze_hwGameModeBase::FillOneSide(const int32 Fl, const int32 Fw, int32 CoordinateOffset)
{
	FRotator RotationWallFl = FRotator(0, 90, 0);
	if (Fl == 0 || Fl == FieldLength - 1)
	{
		float InYfw = 0.f;
		float InXfw = 0.f;
		if (Fl == 0)
		{
			InXfw = (Fl * SizeOneBlock) + CoordinateOffset;
		}
		else
		{
			InXfw = (Fl * SizeOneBlock + SizeOneBlock) + CoordinateOffset;
		}

		InYfw = Fw * SizeOneBlock + (SizeOneBlock / 2);
		const FVector LocationWall = FVector(InXfw, InYfw, 70);

		const auto World = GetWorld();
		if (World && MazeWall)
		{
			AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
			                                                    FActorSpawnParameters());
		}
	}
}

void AMaze_hwGameModeBase::FillWallsForAllCells()
{
	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int elm = 0; elm <= totalCells; elm++)
	{
		auto CurrentCell = AllCells[elm];
		// int32 InX = AllCells[elm].CoordinatX;
		// int32 InY = AllCells[elm].CoordinatY;
		int32 InX = CurrentCell.CoordinatX;
		int32 InY = CurrentCell.CoordinatY;
		float InXwall;
		float InYwall;

		//for Horizontal==================================
		FRotator RotationWallFl = FRotator(0, 0, 0);
		if (InX != FieldWidth)
		{
			if (!CurrentCell.Open_Right)
			{
				const auto World = GetWorld();
				if (World && MazeWall)
				{
					InXwall = InY * SizeOneBlock - (SizeOneBlock / 2);
					InYwall = InX * SizeOneBlock;
					FVector LocationWall = FVector(InXwall, InYwall, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
					                                                    FActorSpawnParameters());
				}
			}
		}

		//for Vertical==================================
		RotationWallFl = FRotator(0, 90, 0);
		if (InY != FieldLength)
		{
			if (!CurrentCell.Open_Down)
			{
				const auto World = GetWorld();
				if (World && MazeWall)
				{
					InXwall = InY * SizeOneBlock;
					InYwall = InX * SizeOneBlock - (SizeOneBlock / 2);
					FVector LocationWall = FVector(InXwall, InYwall, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
					                                                    FActorSpawnParameters());
				}
			}
		}
	}
}

void AMaze_hwGameModeBase::FillWallsForAllCells_v3(int32 CoordinateOffset)
{
	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int elm = 0; elm <= totalCells; elm++)
	{
		auto CurrentCell = AllCellsThread[elm];
		// int32 InX = AllCells[elm].CoordinatX;
		// int32 InY = AllCells[elm].CoordinatY;
		int32 InX = CurrentCell.CoordinatX;
		int32 InY = CurrentCell.CoordinatY;
		float InXwall;
		float InYwall;

		//for Horizontal==================================
		FRotator RotationWallFl = FRotator(0, 0, 0);
		if (InX != FieldWidth)
		{
			if (!CurrentCell.Open_Right)
			{
				const auto World = GetWorld();
				if (World && MazeWall)
				{
					InXwall = InY * SizeOneBlock - (SizeOneBlock / 2) + CoordinateOffset;
					InYwall = InX * SizeOneBlock;
					FVector LocationWall = FVector(InXwall, InYwall, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
																		FActorSpawnParameters());
				}
			}
		}

		//for Vertical==================================
		RotationWallFl = FRotator(0, 90, 0);
		if (InY != FieldLength)
		{
			if (!CurrentCell.Open_Down)
			{
				const auto World = GetWorld();
				if (World && MazeWall)
				{
					InXwall = InY * SizeOneBlock + CoordinateOffset;
					InYwall = InX * SizeOneBlock - (SizeOneBlock / 2);
					FVector LocationWall = FVector(InXwall, InYwall, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
																		FActorSpawnParameters());
				}
			}
		}
	}
}
